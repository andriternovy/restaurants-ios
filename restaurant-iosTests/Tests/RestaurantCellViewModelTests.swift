//
//  RestaurantCellViewModelTests.swift
//  restaurant-iosTests
//
//  Created by Andrii Ternovyi on 9/17/19.
//  Copyright © 2019 restaurant-ios. All rights reserved.
//

import XCTest
@testable import restaurant_ios

class RestaurantCellViewModelTests: XCTestCase {
    
    private var restaurant: Restaurant?
    
    override func setUp() {
        super.setUp()
        let sortOption = SortingOption.init(bestMatch: 1.0, newest: 2.0, ratingAverage: 3.0, distance: 4.0, popularity: 5.0, averageProductPrice: 6.0, deliveryCosts: 7.0, minCost: 8.0)
        restaurant = Restaurant.init(name: "Sushi", status: .open, sortingValues: sortOption)
    }
    
    func testBestMatch() {
        let viewModel = RestaurantCellViewModel(restaurant: restaurant!, sortOption: .bestMatch)
        XCTAssertEqual(viewModel.title, "Sushi")
        XCTAssertEqual(viewModel.sortValue, "1.0")
    }
    
    func testDistance() {
        let viewModel = RestaurantCellViewModel(restaurant: restaurant!, sortOption: .distance)
        XCTAssertEqual(viewModel.title, "Sushi")
        XCTAssertEqual(viewModel.sortValue, "4.0")
    }
    
    func testDeliveryCosts() {
        let viewModel = RestaurantCellViewModel(restaurant: restaurant!, sortOption: .deliveryCosts)
        XCTAssertEqual(viewModel.title, "Sushi")
        XCTAssertEqual(viewModel.sortValue, "7.0")
    }

}
