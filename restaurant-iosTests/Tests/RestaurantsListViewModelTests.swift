//
//  RestaurantsListViewModelTests.swift
//  restaurant-iosTests
//
//  Created by Andrii Ternovyi on 9/17/19.
//  Copyright © 2019 restaurant-ios. All rights reserved.
//

import XCTest
@testable import restaurant_ios
import RxBlocking

class RestaurantsListViewModelTests: XCTestCase {
    
    private var viewModel: RestaurantsListViewModel?
    
    override func setUp() {
        super.setUp()
        viewModel = RestaurantsListViewModel(sortOption: .ratingAverage, dataService: RestaurantMockJsonFileDataService())
    }
    
    func testSearch() {
        viewModel?.startSearch(text: "Lunchpakketdienst")
        XCTAssertEqual(try viewModel?.dataSource.toBlocking().first()?.count, 1)
        
        viewModel?.startSearch(text: "Sushi")
        XCTAssertEqual(try viewModel?.dataSource.toBlocking().first()?.count, 2)
        
        viewModel?.startSearch(text: "Test Test%%%$$$")
        XCTAssertEqual(try viewModel?.dataSource.toBlocking().first()?.count, 0)
        
        viewModel?.startSearch(text: "")
        XCTAssertEqual(try viewModel?.dataSource.toBlocking().first()?.count, 5)
    }
    
    func testDistanceOrder() {
        viewModel?.sortWith(option: .distance)
        let data = try! viewModel?.dataSource.toBlocking().first()!
        XCTAssertEqual(data![0].title, "Sushi One")
        XCTAssertEqual(data![1].title, "De Amsterdamsche Tram")
        XCTAssertEqual(data![2].title, "Lunchpakketdienst")
        XCTAssertEqual(data![3].title, "Lale Restaurant & Snackbar")
        XCTAssertEqual(data![4].title, "Daily Sushi")
    }
    
    func testRatingOrder() {
        viewModel?.sortWith(option: .ratingAverage)
        let data = try! viewModel?.dataSource.toBlocking().first()!
        XCTAssertEqual(data![0].title, "Sushi One")
        XCTAssertEqual(data![1].title, "Lunchpakketdienst")
        XCTAssertEqual(data![2].title, "De Amsterdamsche Tram")
        XCTAssertEqual(data![3].title, "Lale Restaurant & Snackbar")
        XCTAssertEqual(data![4].title, "Daily Sushi")
    }
    
    func testMinCostOrder() {
        viewModel?.sortWith(option: .minCost)
        let data = try! viewModel?.dataSource.toBlocking().first()!
        XCTAssertEqual(data![0].title, "De Amsterdamsche Tram")
        XCTAssertEqual(data![1].title, "Sushi One")
        XCTAssertEqual(data![2].title, "Lunchpakketdienst")
        XCTAssertEqual(data![3].title, "Lale Restaurant & Snackbar")
        XCTAssertEqual(data![4].title, "Daily Sushi")
    }
    
    func testSearchAndOrder() {
        viewModel?.startSearch(text: "Sushi")
        viewModel?.sortWith(option: .distance)
        let data = try! viewModel?.dataSource.toBlocking().first()!
        XCTAssertEqual(data!.count, 2)
        XCTAssertEqual(data![0].title, "Sushi One")
        XCTAssertEqual(data![1].title, "Daily Sushi")
    }
    
}
