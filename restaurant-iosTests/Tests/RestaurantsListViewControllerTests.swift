//
//  RestaurantsListTests.swift
//  restaurant-iosTests
//
//  Created by Andrii Ternovyi on 8/27/19.
//  Copyright © 2019 restaurant-ios. All rights reserved.
//

import XCTest
@testable import restaurant_ios

class RestaurantsListViewControllerTests: XCTestCase {
    
    private var controller: RestaurantsListViewController?
    override func setUp() {
        super.setUp()
        let viewModel = RestaurantsListViewModel(sortOption: .ratingAverage, dataService: RestaurantMockJsonFileDataService())
        controller = RestaurantsListViewController.instantiate(fromStoryboard: .restaurants, viewModel: viewModel)
    }
    
    func testRestaurantsListController() {
        XCTAssertNotNil(controller?.view, "view should not be nil")
        XCTAssertEqual(controller?.content.sortValueTextField.text, "Rating Average ▼ ", "wrong sort option title")
        XCTAssertEqual(controller?.title, "Restaurants", "wrong title")
        
        let sortOptions = controller?.content.sortPicker.numberOfRows(inComponent: 0)
        XCTAssertEqual(sortOptions, 8, "wrong options count")
        
        let rows = controller?.content.tableView.numberOfRows(inSection: 0)
        XCTAssertEqual(rows, 5, "wrong rows count")
    }
    
    
    func testDistanceOrder() {
        controller?.viewModel.sortWith(option: .minCost)
        let cell = controller?.content.tableView.cellForRow(at: IndexPath.init(row: 0, section: 0))
        
        XCTAssert(cell is RestaurantCell, "wrong cell")
        XCTAssertEqual((cell as! RestaurantCell).nameLabel.text, "De Amsterdamsche Tram", "wrong cell title")
    }
}
