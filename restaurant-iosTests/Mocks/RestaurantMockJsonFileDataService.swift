//
//  RestaurantMockJsonFileDataService.swift
//  restaurant-iosTests
//
//  Created by Andrii Ternovyi on 9/17/19.
//  Copyright © 2019 restaurant-ios. All rights reserved.
//

import XCTest
@testable import restaurant_ios

class RestaurantMockJsonFileDataService: RestaurantDataServiceProtocol {
    
    func fetchRestaurants(completion: @escaping ResposeResult<[Restaurant]>) {
        guard let path = Bundle(for: type(of: self)).path(forResource: "Restaurants", ofType: "json") else {
            return completion(nil, NetworkingError.customError("Data not found"))
        }
        
        do {
            let data = try Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
            let jsonDecoder = JSONDecoder()
            let model = try jsonDecoder.decode(Restaurants.self, from: data)
            completion(model.restaurants, nil)
        } catch {
            completion(nil, NetworkingError.errorParsingJSON)
        }
    }
}
