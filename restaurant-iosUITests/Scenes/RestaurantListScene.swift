//
//  RestaurantListScene.swift
//  restaurant-iosUITests
//
//  Created by Andrii Ternovyi on 8/26/19.
//  Copyright © 2019 restaurant-ios. All rights reserved.
//

import XCTest

class RestaurantListScene {
    
    private let factory = BaseTestCase()
    
    func searchText(text: String) {
        
        let searchField = factory.app.descendants(matching: .any).matching(identifier: "searchBar").firstMatch
        factory.waitForElementToAppear(element: searchField)
        searchField.tap()
        searchField.setText(text: text, application: factory.app)
        searchField.tap()
        
        factory.wait(for: 1)
    }
    
    func validate(text: String, result: Int) {
        XCTAssertNotNil(factory.app.cells.staticTexts[text])
        XCTAssertEqual(factory.app.cells.count, result)
    }
    
    func openSortPicker() {
        let textField = factory.app.textFields["sortOptionTextField"]
        textField.tap()
        factory.waitForElementToAppear(element: textField)
    }
    
    func validatePickerValues() {
        let pickerView = factory.app.pickerWheels.element
        let numRows = pickerView.children(matching: .any).count
        XCTAssertEqual(numRows, 8)
        
        let values: [String] = [pickerView.value as! String]
        XCTAssertTrue(values.contains("Best Match"))
    }
    
}
