//
//  XCTestCase_common.swift
//  restaurant-iosUITests
//
//  Created by Andrii Ternovyi on 8/26/19.
//  Copyright © 2019 restaurant-ios. All rights reserved.
//

import XCTest

class BaseTestCase: XCTestCase {
    
    let app = XCUIApplication()
    
    private var launched = false
    override func setUp() {
        super.setUp()
        continueAfterFailure = false
        app.launch()
    }
    
    override func tearDown() {
        super.tearDown()
    }
    
    func waitForElementToAppear(element: XCUIElement) {
        let existsPredicate = NSPredicate(format: "exists == true")
        expectation(for: existsPredicate, evaluatedWith: element, handler: nil)
        waitForExpectations(timeout: 25, handler: nil)
    }
    
    func wait(for time: Double) {
        let wait = expectation(description: "wait")
        DispatchQueue.main.asyncAfter(deadline: .now() + time) {
            wait.fulfill()
        }
        waitForExpectations(timeout: time + 1, handler: nil)
    }
}

extension XCUIElement {
    func setText(text: String, application: XCUIApplication) {
        UIPasteboard.general.string = text
        doubleTap()
        application.menuItems["Paste"].tap()
    }
}
