//
//  RestaurantsList.swift
//  restaurant-iosUITests
//
//  Created by Andrii Ternovyi on 8/26/19.
//  Copyright © 2019 restaurant-ios. All rights reserved.
//

import Foundation

class RestaurantsListTests: BaseTestCase {
    
    func testSearch() {
        let scene = RestaurantListScene()
        let searchText = "Sushi"
        scene.searchText(text: searchText)
        scene.validate(text: searchText, result: 4)
    }
    
    func testSingleSearchResult() {
        let scene = RestaurantListScene()
        let searchText = "Sushi One"
        scene.searchText(text: searchText)
        scene.validate(text: searchText, result: 1)
    }
    
    func testPickerView() {
        let scene = RestaurantListScene()
        scene.openSortPicker()
        scene.validatePickerValues()
    }
}
