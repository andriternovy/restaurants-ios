//
//  RestaurantsListViewController.swift
//  restaurant-ios
//
//  Created by Andrii Ternovyi on 8/25/19.
//  Copyright © 2019 restaurant-ios. All rights reserved.
//

import UIKit
import RxSwift

class RestaurantsListViewController: BaseViewController <RestaurantsListView, RestaurantsListViewModel> {
    
    private let searchController = UISearchController(searchResultsController: nil)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = viewModel.title
        bindSortPicker()
        configureNavigationController()
    }
    
    override func bindViewModel() {
        super.bindViewModel()
        viewModel.dataSource
            .asObservable()
            .subscribe(onNext: {[weak self] (datasource) in
                self?.content.dataSource = datasource
            })
            .disposed(by: viewModel.disposeBag)
        
        viewModel.sortOption
            .asObservable()
            .subscribe(onNext: {[weak self] (sortOption) in
                self?.content.sortValueTextField.text = sortOption.rawValue.capitalized + " ▼ "
            })
            .disposed(by: viewModel.disposeBag)
    }
    
    override func bindContentView() {
        super.bindContentView()
        
        searchController.searchBar.rx.searchButtonClicked
            .asObservable()
            .subscribe(onNext: {[weak self] (_) in
                self?.searchController.searchBar.resignFirstResponder()
            }).disposed(by: viewModel.disposeBag)
        
        searchController.searchBar.rx.text
            .asObservable()
            .subscribe(onNext: {[weak self] (_) in
                self?.viewModel.startSearch(text: self?.searchController.searchBar.text ?? "")
            }).disposed(by: viewModel.disposeBag)
    }
    
    private func configureNavigationController() {
        definesPresentationContext = true
        navigationItem.searchController = searchController
        searchController.dimsBackgroundDuringPresentation = false
        searchController.isActive = true
        searchController.hidesNavigationBarDuringPresentation = false
        navigationItem.hidesSearchBarWhenScrolling = false
        searchController.searchBar.accessibilityIdentifier = "searchBar"
        searchController.searchBar.isAccessibilityElement = true
        searchController.searchBar.accessibilityTraits = UIAccessibilityTraits.searchField
    }
    
    private func bindSortPicker() {
        Observable.just(viewModel.sortOptions)
            .bind(to: content.sortPicker.rx.itemTitles) { _, item in
                return item.rawValue.capitalized
            }
            .disposed(by: viewModel.disposeBag)
        
        content.sortPicker.rx.modelSelected(SortOption.self)
            .subscribe(onNext: {[weak self] models in
                self?.viewModel.sortWith(option: models.first)
            })
            .disposed(by: viewModel.disposeBag)
    }
    
}
