//
//  LocalStorageRepositorie.swift
//  restaurant-ios
//
//  Created by Andrii Ternovyi on 8/26/19.
//  Copyright © 2019 restaurant-ios. All rights reserved.
//

import Foundation
import RxCocoa
import RxSwift

class LocalStorageRepositorie: NSObject {
    
    static let shared = LocalStorageRepositorie()
    private override init() { }
    private let savedFavoritesKey = "savedFavorites"
    
    var savedRestaurantsDataSources: BehaviorRelay<[String]> = BehaviorRelay(value: [])
}

extension LocalStorageRepositorie {
    
    func refreshSavedRestaurants() {
        if let data = UserDefaults.standard.array(forKey: savedFavoritesKey) as? [String] {
            savedRestaurantsDataSources.accept(data)
        }
    }
    
    func save(restaurant: Restaurant) {
        
        let defaults = UserDefaults.standard
        var data = defaults.array(forKey: savedFavoritesKey) as? [String]  ?? []
        data.append(restaurant.name)
        defaults.set(data, forKey: savedFavoritesKey)
        defaults.synchronize()
        
        savedRestaurantsDataSources.accept(data)
    }
    
    func remove(restaurant: Restaurant) {

        let defaults = UserDefaults.standard
        var data = defaults.array(forKey: savedFavoritesKey) as? [String] ?? []
        data = data.filter { $0 != restaurant.name }
        defaults.set( data, forKey: savedFavoritesKey)
        defaults.synchronize()
        
        savedRestaurantsDataSources.accept(data)
    }
    
}
