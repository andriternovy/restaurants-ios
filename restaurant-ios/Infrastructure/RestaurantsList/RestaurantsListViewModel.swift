//
//  RestaurantsListViewModel.swift
//  restaurant-ios
//
//  Created by Andrii Ternovyi on 8/25/19.
//  Copyright © 2019 restaurant-ios. All rights reserved.
//

import UIKit
import RxCocoa

enum SortOption: String, CaseIterable {
    case bestMatch = "best match"
    case newest = "newest"
    case ratingAverage = "rating average"
    case distance = "distance"
    case popularity = "popularity"
    case averageProductPrice = "average product price"
    case deliveryCosts = "delivery costs"
    case minCost = "minimum cost"
}

class RestaurantsListViewModel: BaseViewModel {
    
    private let dataService: RestaurantDataServiceProtocol
    var dataSource: BehaviorRelay<[RestaurantCellViewModel]> = BehaviorRelay(value: [])
    var sortOption: BehaviorRelay<SortOption> = BehaviorRelay(value: .bestMatch)
    private var searchText: String = ""
    
    private var restaurants: [Restaurant] = [] {
        didSet {
            dataSource.accept(restaurants.map { RestaurantCellViewModel(restaurant: $0, sortOption: sortOption.value) })
        }
    }
    
    var title: String {
        return "Restaurants"
    }
    
    required init(sortOption: SortOption, dataService: RestaurantDataServiceProtocol = RestaurantJsonFileDataService()) {
        self.dataService = dataService
        super.init()
        self.sortOption.accept(sortOption)
        self.fetchData()
        
        //remove if don't need resort immediately when added to favorites
        subscribeToFavorites()
    }
    
    required init() {
        fatalError("init() has not been implemented")
    }
    
    private func fetchData() {
        startLoading("Loading...")
        dataService.fetchRestaurants { [weak self] (data, error) in
            self?.finishLoading()
            if let data = data {
                self?.restaurants = data
            } else {
                self?.onErrorMessageVariable.accept(error?.errorDescription)
            }
        }
    }
}

extension RestaurantsListViewModel {
    
    func startSearch(text: String) {
        searchText = text
        updateDataSource()
    }
    
}

extension RestaurantsListViewModel {
    
    var sortOptions: [SortOption] {
        return SortOption.allCases
    }
    
    func sortWith(option: SortOption?) {
        guard let option = option else { return }
        sortOption.accept(option)
        updateDataSource()
    }
    
    private func updateDataSource() {

        var items = searchText.count > 0 ? restaurants.filter  { $0.name.localizedCaseInsensitiveContains(searchText) }  : restaurants
        
        items.sort {
            if self.isSavedLocally(restaurant: $0) != self.isSavedLocally(restaurant: $1) {
                return self.isSavedLocally(restaurant: $0) && !self.isSavedLocally(restaurant: $1)
            }
            
            if $0.status.order != $1.status.order {
                return $0.status.order < $1.status.order
            }
            
            if [SortOption.bestMatch, .newest, .ratingAverage, .popularity].contains(sortOption.value) {
                return $0.valueFor(option: sortOption.value) > $1.valueFor(option: sortOption.value)
            }
            return $0.valueFor(option: sortOption.value) < $1.valueFor(option: sortOption.value)
        }
        dataSource.accept(items.map { RestaurantCellViewModel(restaurant: $0, sortOption: sortOption.value) })
    }
    
    private func subscribeToFavorites() {
        LocalStorageRepositorie.shared.savedRestaurantsDataSources
            .asObservable()
            .subscribe(onNext: { [weak self] savedLocally in
                self?.updateDataSource()
            })
            .disposed(by: disposeBag)
    }
    
    private func isSavedLocally(restaurant: Restaurant) -> Bool {
        let dataSources = LocalStorageRepositorie.shared.savedRestaurantsDataSources
        return dataSources.value.filter { $0 == restaurant.name }.count > 0
    }
}
