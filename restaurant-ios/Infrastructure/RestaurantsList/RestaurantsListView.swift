//
//  RestaurantsListView.swift
//  restaurant-ios
//
//  Created by Andrii Ternovyi on 8/25/19.
//  Copyright © 2019 restaurant-ios. All rights reserved.
//

import UIKit

class RestaurantsListView: BaseView {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var sortValueTextField: UITextField!
    let sortPicker = UIPickerView()

    var dataSource: [RestaurantCellViewModel] = [] {
        didSet {
            tableView.reloadData()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        sortValueTextField.inputView = sortPicker
        createToolbar()
    }
}

extension RestaurantsListView: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let model = dataSource[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: model.identifier, for: indexPath) as? RestaurantCell
        cell?.set(model: model)
        return cell ?? UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataSource.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return dataSource[indexPath.row].cellHeight
    }
    
}

extension RestaurantsListView {
    
    private func createToolbar() {
        let toolbar = UIToolbar()
        toolbar.sizeToFit()
        
        let spacer = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let doneButton = UIBarButtonItem(title: "Done",
                                         style: .plain,
                                         target: self,
                                         action: #selector(RestaurantsListView.hideKeyboard))
        
        toolbar.setItems([spacer, doneButton], animated: false)
        toolbar.isUserInteractionEnabled = true
        sortValueTextField.inputAccessoryView = toolbar
    }
    
    @objc private func hideKeyboard() {
        sortValueTextField.endEditing(true)
    }
}
