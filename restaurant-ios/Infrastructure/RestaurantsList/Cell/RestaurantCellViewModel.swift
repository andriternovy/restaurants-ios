//
//  RestaurantCellViewModel.swift
//  restaurant-ios
//
//  Created by Andrii Ternovyi on 8/26/19.
//  Copyright © 2019 restaurant-ios. All rights reserved.
//

import Foundation
import RxCocoa
import RxSwift

class RestaurantCellViewModel: BaseCellViewModel {
    
    override var identifier: String {
        return "RestaurantCell"
    }
    
    override var cellHeight: CGFloat {
        return 70
    }
    
    var title: String {
        return restaurant.name
    }
    
    var status: String {
        return restaurant.status.rawValue
    }
    
    var sortValue: String?
    
    private let restaurant: Restaurant
    
    var disposeBag = DisposeBag()
    var savedLocally: BehaviorRelay<Bool> = BehaviorRelay(value: false)
    
    init(restaurant: Restaurant, sortOption: SortOption) {
        self.restaurant = restaurant
        super.init()
        self.sortValue = "\(restaurant.valueFor(option: sortOption))"
    }
}

extension RestaurantCellViewModel {
    
    private var isSavedLocally: Bool {
        let dataSources = LocalStorageRepositorie.shared.savedRestaurantsDataSources
        return dataSources.value.filter { $0 == restaurant.name }.count > 0
    }
    
    func subscribeToSaved() {
        LocalStorageRepositorie.shared.savedRestaurantsDataSources
            .map { _ in self.isSavedLocally }
            .bind(to: savedLocally)
            .disposed(by: disposeBag)
    }
    
    func updatedSavedStatus() {
        if isSavedLocally {
            LocalStorageRepositorie.shared.remove(restaurant: restaurant)
        } else {
            LocalStorageRepositorie.shared.save(restaurant: restaurant)
        }
    }
}
