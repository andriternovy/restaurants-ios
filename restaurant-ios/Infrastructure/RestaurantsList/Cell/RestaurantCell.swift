//
//  RestaurantCell.swift
//  restaurant-ios
//
//  Created by Andrii Ternovyi on 8/25/19.
//  Copyright © 2019 restaurant-ios. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift

class RestaurantCell: UITableViewCell {

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var statusLabel: UILabel!
    @IBOutlet weak var sortValueLabel: UILabel!
    @IBOutlet weak var addToFavorites: UIButton!
    
    override func prepareForReuse() {
        super.prepareForReuse()
        nameLabel.text = nil
        statusLabel.text = nil
        sortValueLabel.text = nil
        model?.disposeBag = DisposeBag()
    }
    
    var model: RestaurantCellViewModel?

    func set<T>(model: T) where T: RestaurantCellViewModel {
        
        self.model = model
        
        nameLabel.text = model.title
        statusLabel.text = model.status
        sortValueLabel.text = model.sortValue
        
        model.subscribeToSaved()
        
        model.savedLocally
            .asObservable()
            .subscribe(onNext: { [weak self] savedLocally in
                self?.addToFavorites.isSelected = savedLocally
            })
            .disposed(by: model.disposeBag)
        
        addToFavorites.rx
            .tap
            .subscribe(onNext: { _ in
                model.updatedSavedStatus()
            })
            .disposed(by: model.disposeBag)
    }
}
