//
//  Storyboard.swift
//  restaurant-ios
//
//  Created by Andrii Ternovyi on 8/25/19.
//  Copyright © 2019 restaurant-ios. All rights reserved.
//

import Foundation

public enum Storyboard: String {
    case restaurants = "Restaurants"
}

