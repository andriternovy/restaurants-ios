//
//  AppDelegate.swift
//  restaurant-ios
//
//  Created by Andrii Ternovyi on 8/25/19.
//  Copyright © 2019 restaurant-ios. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        LocalStorageRepositorie.shared.refreshSavedRestaurants()
        
        window = UIWindow(frame: UIScreen.main.bounds)
        
        let vm = RestaurantsListViewModel(sortOption: .distance)
        let vc = RestaurantsListViewController.instantiate(fromStoryboard: .restaurants, viewModel: vm)
        self.window?.rootViewController = UINavigationController(rootViewController: vc)
        self.window?.makeKeyAndVisible()
        
        return true
    }
    
}
