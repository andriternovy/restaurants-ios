//
//  OrderStatus.swift
//  restaurant-ios
//
//  Created by Andrii Ternovyi on 8/26/19.
//  Copyright © 2019 restaurant-ios. All rights reserved.
//

import Foundation

enum OrderStatus: String, Codable {
    case open
    case closed
    case orderahead = "order ahead"
}

extension OrderStatus {
    var order: Int {
        switch self {
        case .open:
            return 0
        case .closed:
            return 2
        case .orderahead:
            return 1
        }
    }
}
