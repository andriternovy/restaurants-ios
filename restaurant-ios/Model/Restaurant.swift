//
//  Restaurant.swift
//  restaurant-ios
//
//  Created by Andrii Ternovyi on 8/25/19.
//  Copyright © 2019 restaurant-ios. All rights reserved.
//

import Foundation

struct Restaurants: Codable {
    let restaurants: [Restaurant]
}

struct Restaurant: Codable {
    let name: String
    let status: OrderStatus
    let sortingValues: SortingOption
}

struct SortingOption: Codable {
    let bestMatch: Double
    let newest: Double
    let ratingAverage: Double
    let distance: Double
    let popularity: Double
    let averageProductPrice: Double
    let deliveryCosts: Double
    let minCost: Double
}

extension Restaurant {
    
    func valueFor(option: SortOption) -> Double {
        var result: Double = 0
        switch option {
        case .bestMatch: result = self.sortingValues.bestMatch
        case .newest: result = self.sortingValues.newest
        case .ratingAverage: result = self.sortingValues.ratingAverage
        case .distance: result = self.sortingValues.distance
        case .popularity: result = self.sortingValues.popularity
        case .averageProductPrice: result = self.sortingValues.averageProductPrice
        case .deliveryCosts: result = self.sortingValues.deliveryCosts
        case .minCost: result = self.sortingValues.minCost
        }
        return result
    }
}
