//
//  BaseCellViewModel.swift
//  restaurant-ios
//
//  Created by Andrii Ternovyi on 8/25/19.
//  Copyright © 2019 restaurant-ios. All rights reserved.
//

import RxSwift

class BaseCellViewModel: NSObject {
    
    var identifier: String {
        fatalError("Reload this in child")
    }
    
    var cellHeight: CGFloat {
        return UITableView.automaticDimension
    }
    
}
