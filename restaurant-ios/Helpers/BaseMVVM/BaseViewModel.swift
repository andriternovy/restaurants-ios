//
//  BaseViewModel.swift
//  restaurant-ios
//
//  Created by Andrii Ternovyi on 8/25/19.
//  Copyright © 2019 restaurant-ios. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

class BaseViewModel {
    
    var disposeBag = DisposeBag()
    
    var onErrorMessageVariable: BehaviorRelay<String?> = BehaviorRelay(value: nil)
    
    var observerErrorsMessage: Observable<String> {
        return onErrorMessageVariable
            .asObservable()
            .filter({ $0 != nil })
            .map({ $0! })
    }
    
    private var loadingMessageVariable: BehaviorRelay<String?> = BehaviorRelay(value: nil)
    private var loadingCounterVariable: BehaviorRelay<Int> = BehaviorRelay(value: 0)
    
    var shouldShowLoading: Observable<(show: Bool, message: String)> {
        return Observable<(show: Bool, message: String)>
            .combineLatest(
                loadingCounterVariable
                    .asObservable()
                    .map({ $0 > 0 }),
                loadingMessageVariable.asObservable()
                    .map({ $0 ?? "Loading" })) { show, msg in
                        return (show, msg)
        }
    }
    
    required init() {
        
    }
    
    func startLoading(_ message: String? = nil) {
        loadingMessageVariable.accept(message)
        loadingCounterVariable.accept(loadingCounterVariable.value + 1)
    }
    
    func finishLoading() {
        var count = loadingCounterVariable.value - 1
        count = max(count, 0)
        loadingCounterVariable.accept(count)
    }
}
