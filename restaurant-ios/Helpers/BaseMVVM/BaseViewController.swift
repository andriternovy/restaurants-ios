//
//  BaseViewController.swift
//  restaurant-ios
//
//  Created by Andrii Ternovyi on 8/25/19.
//  Copyright © 2019 restaurant-ios. All rights reserved.
//

import UIKit

class BaseViewController<Content: BaseView, ViewModel: BaseViewModel>: UIViewController {
    var content: Content {
        return self.view as! Content
    }
    private(set) var viewModel: ViewModel!
    
    class func instantiate(fromStoryboard storyboard: Storyboard, viewModel: ViewModel) -> Self {
        let vc = self.instantiate(storyboard: storyboard)
        if let vc = vc as BaseViewController? {
            vc.viewModel = viewModel
        }
        return vc
    }
    
    func update(viewModel: ViewModel) {
        self.viewModel = viewModel
        bindViewModel()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if viewModel != nil {
            bindViewModel()
        }
        bindContentView()
    }
    
    func bindViewModel() {
        viewModel.observerErrorsMessage
            .subscribe(onNext: {[weak self] (error) in
                self?.showOkAlert(body: error)
            })
            .disposed(by: viewModel.disposeBag)
        
        viewModel.shouldShowLoading
            .subscribe(onNext: { [weak self] (isLoading, msg) in
                if isLoading {
                    self?.content.showActivity(msg)
                } else {
                    self?.content.hideActivity()
                }
            })
            .disposed(by: viewModel.disposeBag)
    }
    
    func bindContentView() {
    }
        
}
