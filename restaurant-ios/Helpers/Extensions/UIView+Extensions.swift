//
//  UIView+Extensions.swift
//  ios-last-fm
//
//  Created by Andrii Ternovyi on 7/20/19.
//  Copyright © 2019 Andrii Ternovyi. All rights reserved.
//

import UIKit

extension UIView {
    
    struct Holder {
        static var activityView: LoadingView?
    }
    
    var activityView: LoadingView? {
        get {
            return Holder.activityView
        }
        set(newValue) {
            Holder.activityView = newValue
        }
    }
    
    func showActivity(_ string: String) {
        
        if activityView == nil {
            activityView = Bundle.main.loadNibNamed("LoadingView", owner: self, options: nil)?.first as? LoadingView
        }
        
        if let activityView = activityView, activityView.superview == nil {
            activityView.isHidden = false
            activityView.frame = bounds
            activityView.textLabel.text = string
            activityView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
            addSubview(activityView)
            activityView.spinner.startAnimating()
        }
    }
    
    func hideActivity() {
        if let activityView = activityView {
            activityView.spinner.stopAnimating()
            activityView.removeFromSuperview()
            Holder.activityView = nil
        }
        
    }
}
