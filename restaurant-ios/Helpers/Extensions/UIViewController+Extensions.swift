//
//  UIViewController+Extensions.swift
//  restaurant-ios
//
//  Created by Andrii Ternovyi on 8/25/19.
//  Copyright © 2019 restaurant-ios. All rights reserved.
//

import UIKit

extension UIViewController {
    
    class var identifier: String {
        return String(describing: self)
    }
    
    class func instantiate(storyboard: Storyboard) -> Self {
        return instantiateFromStoryboardHelper(type: self, storyboardName: storyboard.rawValue)
    }
    
    class func instantiate(storyboardName: String) -> Self {
        return instantiateFromStoryboardHelper(type: self, storyboardName: storyboardName)
    }
    
    private class func instantiateFromStoryboardHelper<T>(type: T.Type, storyboardName: String) -> T {
        let storyboardID = identifier
        let storyboard = UIStoryboard(name: storyboardName.description, bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: storyboardID) as! T
        return controller
    }
    
    func showOkAlert(title: String = "", body: String, okButtonTitle: String = "OK", successCallback: (() -> Void)? = nil) {
        let alertController = UIAlertController(title: title, message: body, preferredStyle: .alert)
        
        let okAction = UIAlertAction(title: okButtonTitle, style: .default) { (_: UIAlertAction) -> Void in
            successCallback?()
        }
        
        alertController.addAction(okAction)
        self.present(alertController, animated: true, completion: nil)
    }
}

