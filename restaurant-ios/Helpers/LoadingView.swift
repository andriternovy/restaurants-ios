//
//  LoadingView.swift
//  restaurant-ios
//
//  Created by Andrii Ternovyi on 8/25/19.
//  Copyright © 2019 restaurant-ios. All rights reserved.
//


import UIKit

class LoadingView: UIView {

    @IBOutlet weak var textLabel: UILabel!
    @IBOutlet weak var spinner: UIActivityIndicatorView!

}
