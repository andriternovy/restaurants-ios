//
//  NetworkingError.swift
//  restaurant-ios
//
//  Created by Andrii Ternovyi on 8/25/19.
//  Copyright © 2019 restaurant-ios. All rights reserved.
//

import Foundation

enum NetworkingError: Error {
    case errorParsingJSON
    case customError(String)
    case nsError(NSError)
}

extension NetworkingError: LocalizedError {
    var errorDescription: String? {
        switch self {
        case .errorParsingJSON:
            return "Error Parsing Data"
        case .customError(let err):
            return err
        default:
            return "Error. Please try again later"
        }
    }
}
