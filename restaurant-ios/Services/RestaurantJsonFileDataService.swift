//
//  RestaurantJsonFileDataService.swift
//  restaurant-ios
//
//  Created by Andrii Ternovyi on 8/25/19.
//  Copyright © 2019 restaurant-ios. All rights reserved.
//

import Foundation

typealias ResposeResult<T: Codable> = (T?, NetworkingError?) -> Void

protocol RestaurantDataServiceProtocol {
    func fetchRestaurants(completion: @escaping ResposeResult<[Restaurant]>)
}

class RestaurantJsonFileDataService: RestaurantDataServiceProtocol {
    
    func fetchRestaurants(completion: @escaping ResposeResult<[Restaurant]>) {
        guard let path = Bundle.main.path(forResource: "Restaurants", ofType: "json") else {
            return completion(nil, NetworkingError.customError("Data not found"))
        }
        
        do {
            let data = try Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
            let jsonDecoder = JSONDecoder()
            let model = try jsonDecoder.decode(Restaurants.self, from: data)
            completion(model.restaurants, nil)
        } catch {
            completion(nil, NetworkingError.errorParsingJSON)
        }
    }
    
}



